volatile bool charging = false;

extern void display_refresh(bool display_co2);
extern void sample_voltage();
extern float vbat;

void battery_setup() {
  // Enable comparator for detecting USB voltage

    GCLK->CLKCTRL.reg = 0x4220;                     //enable GGCLK for AC_ana, CLKGEN2 = 32 kHz ultra low power internal oscillator
    while (GCLK->STATUS.bit.SYNCBUSY); 
    REG_GCLK_CLKCTRL = 0x421F;                      //enable GGCLK for AC_dig, CLKGEN2 = 32 kHz ultra low power internal oscillator
    while (GCLK->STATUS.bit.SYNCBUSY);
       
    REG_PM_APBCMASK |= PM_APBCMASK_AC;              //switch on AC module in ppower manager

    NVIC_EnableIRQ(AC_IRQn); 
    
    REG_AC_SCALER0 |= AC_SCALER_VALUE(0x28);        //set output of VDDA scaler set to 
        
    REG_AC_COMPCTRL0 |= AC_COMPCTRL_HYST;
    REG_AC_COMPCTRL0 |= AC_COMPCTRL_INTSEL_TOGGLE;  // interrupt in toggle mode
    REG_AC_COMPCTRL0 |= AC_COMPCTRL_MUXPOS_PIN0;
    REG_AC_COMPCTRL0 |= AC_COMPCTRL_MUXNEG_VSCALE;
    REG_AC_COMPCTRL0 |= AC_COMPCTRL_FLEN_MAJ5;
    REG_AC_COMPCTRL0 |= AC_COMPCTRL_ENABLE;         // comperator0 enabled 
    while (AC->STATUSB.bit.SYNCBUSY);

    REG_AC_INTENSET |= AC_INTENSET_COMP0;           // COMP0 interrupt enabled

    REG_AC_CTRLA |= AC_CTRLA_LPMUX;                 // low power multiplexer
    REG_AC_CTRLA |= AC_CTRLA_RUNSTDBY(0b01);        // Comparator runs during sleep
    REG_AC_CTRLA |= AC_CTRLA_ENABLE;                // AC module enabled 
    while (AC->STATUSB.bit.SYNCBUSY);
}

void AC_Handler() {
  delayMicroseconds(20000);
  if (REG_AC_STATUSA & AC_STATUSA_STATE0) {
    charging = true;
    DOG.picture(88, 6, battery_charging);
  } else {
    charging = false;
    vbat = sample_voltage(VBATPIN);
    if(vbat >= 3.6) {
      DOG.picture(88, 6, battery_full);
    } else if(vbat >= 3.5) {
      DOG.picture(88, 6, battery_good);
    } else if(vbat >= 3.4) {
      DOG.picture(88, 6, battery_low);
    } else {
      DOG.picture(88, 6, battery_empty);
    }
  }
  noInterrupts();
  if (RTC->MODE2.Mode2Alarm[0].MASK.bit.SEL != 0x00) {
    SCB->SCR |= SCB_SCR_SLEEPONEXIT_Msk;
  }
  interrupts();
  REG_AC_INTFLAG |= AC_INTFLAG_COMP0;
}
