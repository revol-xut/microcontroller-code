// This EUI must be in little-endian format, so least-significant-byte
// first. When copying an EUI from ttnctl output, this means to reverse
// the bytes. For TTN issued EUIs the last bytes should be 0xD5, 0xB3,
// 0x70.
const u1_t APPEUI[8]={0x00, 0x00,0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
void os_getArtEui (u1_t* buf) {memcpy_P(buf, APPEUI, 8);}

// This should also be in little endian format, see above. (von hinten nach vorne)
const u1_t DEVEUI[8]={0xec, 0x05, 0xbd, 0x2a, 0x70, 0xa8, 0xdb, 0x37};
void os_getDevEui (u1_t* buf) {memcpy_P(buf, DEVEUI, 8);}

// This key should be in big endian (MSB) format (or, since it is not really a
// number but a block of memory, endianness does not really apply). In
// practice, a key taken from ttnctl can be copied as-is.
const u1_t APPKEY[16] = {0xbd, 0x09, 0x95, 0x36, 0xbe, 0x8d, 0xfd, 0xdc, 0x49, 0x86, 0x2e, 0xca, 0x9b, 0x87, 0xd2, 0x5c};
void os_getDevKey (u1_t* buf) {memcpy_P(buf, APPKEY, 16);}

static osjob_t sendjob;

void printHex2(unsigned v) {
    v &= 0xff;
    if (v < 16)
        Serial.print('0');
    Serial.print(v, HEX);
}

void do_send(osjob_t* j){

    char floatStr[16];

    // Check if there is not a current TX/RX job running
    if (LMIC.opmode & OP_TXRXPEND) {
        Serial.println(F("OP_TXRXPEND, not sending"));
    } else {
        // Prepare upstream data transmission at the next possible time.

        unsigned int v = (int)(vbat * 1000);  // convert to mV
        unsigned int t = (unsigned int)((temperature + 40.05) * 10.0); 
        // t = t + 40; => t [-40..+85] => [0..125] => t = t * 10; => t [0..125] => [0..1250] => round to 0.1 degree
        unsigned int p = (unsigned int) pressure;  // p [300..1100] |hPa]
        unsigned int h = (unsigned int)((humidity + 0.05) * 10.0);   // 0 ... 100%
        unsigned int c = (unsigned int)(co2);  // CO2 level [400..10000]

        unsigned char mydata[10];
        mydata[0] = (v >> 8) & 0xFF;
        mydata[1] =  v       & 0xFF;
        mydata[2] = (t >> 8) & 0xFF;      
        mydata[3] =  t       & 0xFF; 
        mydata[4] = (p >> 8) & 0xFF;
        mydata[5] =  p       & 0xFF;
        mydata[6] = (h >> 8) & 0xFF;
        mydata[7] =  h       & 0xFF; 
        mydata[8] = (c >> 8) & 0xFF;
        mydata[9] =  c       & 0xFF; 

        LMIC_setTxData2(68, mydata, sizeof(mydata), 0);
        Serial.println(F("Packet queued"));
    }
    // Next TX is scheduled after TX_COMPLETE event.
}

void onEvent (ev_t ev) {

    unsigned long tx_end, meas_end;
    
    Serial.print(os_getTime());
    Serial.print(": ");
    switch(ev) {
        case EV_SCAN_TIMEOUT:
            Serial.println(F("EV_SCAN_TIMEOUT"));
            break;
        case EV_BEACON_FOUND:
            Serial.println(F("EV_BEACON_FOUND"));
            break;
        case EV_BEACON_MISSED:
            Serial.println(F("EV_BEACON_MISSED"));
            break;
        case EV_BEACON_TRACKED:
            Serial.println(F("EV_BEACON_TRACKED"));
            break;
        case EV_JOINING:
            Serial.println(F("EV_JOINING"));
            break;
        case EV_JOINED:
            Serial.println(F("EV_JOINED"));
            {
              u4_t netid = 0;
              devaddr_t devaddr = 0;
              u1_t nwkKey[16];
              u1_t artKey[16];
              LMIC_getSessionKeys(&netid, &devaddr, nwkKey, artKey);
              Serial.print("netid: ");
              Serial.println(netid, DEC);
              Serial.print("devaddr: ");
              Serial.println(devaddr, HEX);
              Serial.print("AppSKey: ");
              for (size_t i=0; i<sizeof(artKey); ++i) {
                if (i != 0)
                  Serial.print("-");
                printHex2(artKey[i]);
              }
              Serial.println("");
              Serial.print("NwkSKey: ");
              for (size_t i=0; i<sizeof(nwkKey); ++i) {
                      if (i != 0)
                              Serial.print("-");
                      printHex2(nwkKey[i]);
              }
              Serial.println();
            }
            // Disable link check validation (automatically enabled
            // during join, but because slow data rates change max TX
            // size, we don't use it in this example.
            LMIC_setLinkCheckMode(0);
            break;
        /*
        || This event is defined but not used in the code. No
        || point in wasting codespace on it.
        ||
        || case EV_RFU1:
        ||     Serial.println(F("EV_RFU1"));
        ||     break;
        */
        case EV_JOIN_FAILED:
            Serial.println(F("EV_JOIN_FAILED"));
            break;
        case EV_REJOIN_FAILED:
            Serial.println(F("EV_REJOIN_FAILED"));
            break;
        case EV_TXCOMPLETE:
            Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
            if (LMIC.txrxFlags & TXRX_ACK)
              Serial.println(F("Received ack"));
            if (LMIC.dataLen) {
              Serial.print(F("Received "));
              Serial.print(LMIC.dataLen);
              Serial.println(F(" bytes of payload"));
            }
            // Schedule next transmission
//            os_setTimedCallback(&sendjob, os_getTime()+sec2osticks(TX_INTERVAL), do_send);
            do_send(&sendjob);
            tx_count += MEAS_INTERVAL;
            break;
        case EV_LOST_TSYNC:
            Serial.println(F("EV_LOST_TSYNC"));
            break;
        case EV_RESET:
            Serial.println(F("EV_RESET"));
            break;
        case EV_RXCOMPLETE:
            // data received in ping slot
            Serial.println(F("EV_RXCOMPLETE"));
            break;
        case EV_LINK_DEAD:
            Serial.println(F("EV_LINK_DEAD"));
            break;
        case EV_LINK_ALIVE:
            Serial.println(F("EV_LINK_ALIVE"));
            break;
        /*
        || This event is defined but not used in the code. No
        || point in wasting codespace on it.
        ||
        || case EV_SCAN_FOUND:
        ||    Serial.println(F("EV_SCAN_FOUND"));
        ||    break;
        */
        case EV_TXSTART:
            Serial.println(F("EV_TXSTART"));
            break;
        case EV_TXCANCELED:
            Serial.println(F("EV_TXCANCELED"));
            break;
        case EV_RXSTART:
            /* do not print anything -- it wrecks timing */
            break;
        case EV_JOIN_TXCOMPLETE:
            Serial.println(F("EV_JOIN_TXCOMPLETE: no JoinAccept"));
            do_send(&sendjob);
            tx_count += MEAS_INTERVAL;  //Retransmit
            break;

        default:
            Serial.print(F("Unknown event: "));
            Serial.println((unsigned) ev);
            break;
    }
}

inline void lorawan_init() {
  os_init();
  LMIC_reset();
  LMIC_setClockError(MAX_CLOCK_ERROR * 10 / 100);
  do_send(&sendjob);
}
