#include <Adafruit_NeoPixel.h>
#include <Adafruit_BME280.h>
#include <avr/dtostrf.h>
#include <ArduinoLowPower.h>
#include <DallasTemperature.h>
#include <DogGraphicDisplay.h>
#include <lmic.h>
#include <hal/hal.h>
#include <OneWire.h>
#include <SPI.h>
#include <Wire.h> 
#include <SparkFun_SCD30_Arduino_Library.h>

#include "ubuntumono_b_16.h"
#include "Symbols.h"

#define VOLTAGE_SAMPLING_COUNT 3

#define BUTTON_LEFT     11
#define BUTTON_CENTER   12
#define BUTTON_RIGHT    15
#define BUZZER          1
#define ONE_WIRE_BUS    10
#define PIXELPIN        16
#define VBATPIN         A7

#define TX_INTERVAL   600
#define MEAS_INTERVAL 60
#define CO2_INTERVAL  180

DogGraphicDisplay DOG;
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
DeviceAddress Thermometer;
Adafruit_NeoPixel pixels(1, PIXELPIN, NEO_GRB + NEO_KHZ800);
Adafruit_BME280 bme280;
SCD30 airSensor;

float temperature = 0, humidity = 0, pressure = 0, vbat = 0;
uint16_t co2 = 0;
int tx_count = 0;
bool display_sleep = false, buzzer_on = false, led_on = false;
uint32_t led_color = 0;

#include "Lorawan.h"  //Needs global vars

const lmic_pinmap lmic_pins = {
    .nss = 8,
    .rxtx = LMIC_UNUSED_PIN,
    .rst = 4,
    .dio = {3, 6, 5},
};

float sample_voltage(int pin) {
  float volt = 0;
  for(int i = 0; i < VOLTAGE_SAMPLING_COUNT; i++) {
    volt += analogRead(pin);
  }
  volt /= VOLTAGE_SAMPLING_COUNT; //Average voltage
  volt *= 2;    // we divided by 2, so multiply back
  volt *= 3.3;  // Multiply by 3.3V, our reference voltage
  volt /= 1024; // convert to voltage
  return volt;
}

#include "Battery.h"

void button_left() {  //Display
  delayMicroseconds(20000);
  if(!digitalRead(BUTTON_LEFT)) {
    if(display_sleep) {
      display_sleep = false;
      DOG.sleep(false);
    } else {
      display_sleep = true;
      DOG.sleep(true);
    }
  }
  noInterrupts();
  if (RTC->MODE2.Mode2Alarm[0].MASK.bit.SEL != 0x00) {
    SCB->SCR |= SCB_SCR_SLEEPONEXIT_Msk;
  }
  interrupts();
}

void button_center() {  //Buzzer
  delayMicroseconds(20000);
  if(!digitalRead(BUTTON_CENTER)) {
    if(buzzer_on) {
      buzzer_on = false;
      DOG.picture(72, 6, buzzer_off);
    } else {
      buzzer_on = true;
      DOG.picture(72, 6, buzzer_active);
    }
  }
  noInterrupts();
  if (RTC->MODE2.Mode2Alarm[0].MASK.bit.SEL != 0x00) {
    SCB->SCR |= SCB_SCR_SLEEPONEXIT_Msk;
  }
  interrupts();
}

void button_right() { //LED
  delayMicroseconds(20000);
  if(!digitalRead(BUTTON_RIGHT)) {  // Knopf gedrückt, Pegel am Pin LOW
    if(led_on) {
      led_on = false;
      pixels.clear();
    } else {
      led_on = true;
      pixels.setPixelColor(0, led_color);
    }
    pixels.show();
  }
  noInterrupts();
  if (RTC->MODE2.Mode2Alarm[0].MASK.bit.SEL != 0x00) {
    SCB->SCR |= SCB_SCR_SLEEPONEXIT_Msk;
  }
  interrupts();
}

void RTC_isr() {
  noInterrupts();
  RTC->MODE2.Mode2Alarm[0].MASK.bit.SEL = 0x00;
  while (RTC->MODE2.STATUS.bit.SYNCBUSY);
  SCB->SCR &= ~SCB_SCR_SLEEPONEXIT_Msk;
  interrupts();  
}

inline void display_refresh() {
  //Temperature
  char str[7] = {0};
  if(temperature >= 100) { //Prevent Buffer Overflow
    temperature = 99;
  } else if(temperature <= -100) {
    temperature = -99;
  }
  dtostrf(temperature, 3, 0, str);
  strcat(str, " *C");
  DOG.string(0, 1, UBUNTUMONO_B_16, str, ALIGN_RIGHT);  //1st row

  //Humidity (reuses str[])
  if(humidity >= 100) { //Prevent Buffer Overflow
    humidity = 99;
  } else if(humidity < 0) {
    humidity = 0;
  }
  dtostrf(humidity, 3, 0, str);
  strcat(str, " % ");
  DOG.string(0, 3, UBUNTUMONO_B_16, str, ALIGN_RIGHT);  //3rd row

  //CO2 face
  if(co2 >= 2000) {
    DOG.picture(16, 5, sad);
    led_color = pixels.Color(0, 255, 0);
    if(buzzer_on) {
      tone(BUZZER, 4500);
      delay(500);
      noTone(BUZZER);
    }
  } else if(co2 >= 1000) {
    DOG.picture(16, 5, neutral);
    led_color = pixels.Color(255, 255, 00);
  } else {
    DOG.picture(16, 5, happy);
    led_color = pixels.Color(255, 0, 0);
  }
  if(led_on) {
    pixels.setPixelColor(0, led_color);
    pixels.show();
  }
}

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(BUTTON_LEFT, INPUT_PULLUP);
  pinMode(BUTTON_CENTER, INPUT_PULLUP);
  pinMode(BUTTON_RIGHT, INPUT_PULLUP);
  pinMode(BUZZER, OUTPUT);

  pixels.begin();
  pixels.clear();
  pixels.show();

  sensors.begin();
  sensors.getAddress(Thermometer, 0);

  bme280.begin(0x76);

  Wire.begin();
  Wire.setClock(100000);

  airSensor.begin();
  airSensor.reset();
  airSensor.setMeasurementInterval(4);
  airSensor.setAmbientPressure(bme280.readPressure());

  Serial.begin(115200);
  LowPower.attachInterruptWakeup(RTC_ALARM_WAKEUP, RTC_isr, (irq_mode) 0);
  RTC->MODE2.Mode2Alarm[0].MASK.bit.SEL = 0x00;
  while (RTC->MODE2.STATUS.bit.SYNCBUSY);
  LowPower.attachInterruptWakeup(BUTTON_LEFT, button_left, CHANGE);
  LowPower.attachInterruptWakeup(BUTTON_CENTER, button_center, CHANGE);
  LowPower.attachInterruptWakeup(BUTTON_RIGHT, button_right, CHANGE);

  DOG.begin(14, 0, 0, 19, 18, DOGM128);   //CS = 17, 0,0= use Hardware SPI, A0 = 19, RESET = 18, EA DOGM128-6 (=128x64 dots)
  DOG.clear();
  DOG.picture(0, 0, smiley);
  DOG.picture(16, 5, happy);
  DOG.picture(72, 1, temperature_icon);
  DOG.picture(72, 3, humidity_icon);

  battery_setup();
  analogRead(VBATPIN); //First conversation might be faulty

  sensor_get();
  display_refresh();

  lorawan_init();
}

inline void sensor_get() {
  sensors.requestTemperatures();
  temperature = sensors.getTempC(Thermometer);

  pressure = bme280.readPressure() / 100.0f;
  humidity = bme280.readHumidity();

  if(airSensor.dataAvailable()) {
    co2 = airSensor.getCO2();
    airSensor.setAmbientPressure(pressure);
  }
  vbat = sample_voltage(VBATPIN);
}

void loop() {
  if(tx_count == 0) {
    os_runloop_once();
  } else {
    uint32_t t_corr = millis();     // correction due to delay from display_set() / sensor_loop()
    if (tx_count >= TX_INTERVAL) {
      tx_count = 0;
    } else {
      tx_count += MEAS_INTERVAL;
    }
    sensor_get();
    display_refresh();
    LowPower.sleep((uint32_t) (MEAS_INTERVAL*1000) - (millis() - t_corr) - 1200);
  }
}
