Climate Tracker with LoRaWAN - Microcontroller Code
---------------------------------------------

![](https://img.shields.io/bitbucket/pipelines/revol-xut/microcontroller-code?style=flat-square) 

**Contact**: <robo-armin@gmx.de>, <revol-xut@protonmail.com>

This repository contains the source code which collects the data from the sensors and sends it to the LuraWan Gateway.

### Abstract

The LoRaWAN sensor node uploads humidity, temperature, co2 and battery data regularly to a preconfigured
ChirpStack server. It also displays them on his LCD screen in a user-friendly fashion.
A Adafruit Feather M0 LoRaWAN is used for processing, together with a 128x64 LCD,
a SCD30 air sensor and a dallas temperature sensor.

### Compilation & Deployment

1. Install the Arduino IDE and [setup Adafruit Feather Support](https://learn.adafruit.com/adafruit-feather-m0-radio-with-lora-radio-module/setup).

2. Use the library manager to add the DOGM Dislpay library (only aviable as .zip).

3. Install the Sparkfun SCD30 and the Dallas temperature library.

3. [Upload](https://learn.adafruit.com/adafruit-feather-m0-radio-with-lora-radio-module/using-with-arduino-ide) firmware/firmware.ino onto the Board (double press the reset button for entering ISP mode).

### Project Structure

firware/* contains the executable code.
